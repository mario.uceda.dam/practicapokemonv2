package practicapokemon;
public class Charizard extends Pokemon implements Pokemontura {
    
    public Charizard(int Nivel, String Mote){
        this.Nivel=Nivel;
        this.Mote=Mote;
        this.Ataque=Nivel*5;
        this.Defensa=Nivel*6;
        this.Ps=Nivel*6;
        this.Especie="Charizard";
        this.Tipo="Fuego";
    }
    public Charizard (int Nivel){
        this.Nivel=Nivel;
        this.Especie="Charizard";
        this.Mote=Especie;
        this.Ataque=Nivel*5;
        this.Defensa=Nivel*6;
        this.Ps=Nivel*5;
        this.Tipo="Fuego";
    }
    public String Montar(){
        return ("te montaste en el " + this.Mote);
    }
}
