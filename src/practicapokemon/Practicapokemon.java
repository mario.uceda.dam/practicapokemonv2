package practicapokemon;
import java.util.Scanner;
public class Practicapokemon {
    //metodo para mostrar texto cada vez que capturas un pokemon
    public static void capturapokemon(Pokemon patata){
        Scanner teclado = new Scanner (System.in);
        System.out.println("POKEMON CAPTURADO!!!");
        System.out.println("====================");
        System.out.println("Especie: "+ patata.getEspecie() );
        System.out.println("Nivel: "+ patata.getNivel());
        System.out.println("¿Desea ponerle algún mote?");
        String Respuesta=teclado.nextLine();
        if (Respuesta.equals("si")||Respuesta.equals("SI")){
            System.out.println("Introduce el nuevo mote");
            String nuevoMote= teclado.nextLine();
            patata.setMote(nuevoMote);
        }
        System.out.println("mote: "+patata.getMote());   
    }
    
    public static void main(String[] args) {
        Scanner teclado= new Scanner(System.in);
        String opcion1;
        Mochila pokeMochila = new Mochila();
        do{
            System.out.println("MENU");
            System.out.println("====");
            System.out.println("1) Capturar pokémon");
            System.out.println("2) Listar pokémon");
            System.out.println("0) Salir");
            System.out.print("Opción: ");
            opcion1=teclado.nextLine();
            while (!opcion1.equals("0") && !opcion1.equals("1") && !opcion1.equals("2")){
                System.out.println("Introduce una opción correcta");
                opcion1=teclado.nextLine();
            }
            switch(opcion1){
                
                case("1"):
                    
                        int numero = (int) (Math.random() * 4) + 1;

                        if (numero==1){
                            Bulbasaur bulbasaur = new Bulbasaur((int) (Math.random() * 100) + 1);
                            try{pokeMochila.capturarPokemon(bulbasaur);
                               capturapokemon(bulbasaur); 
                            }
                            catch(MochilaLlenaException error){
                                System.err.println(error.toString());
                            }
                            
                        }
                        else {
                            if (numero==2){
                                Mewtwo miutu = new Mewtwo((int) (Math.random() * 100) + 1);
                            try{pokeMochila.capturarPokemon(miutu);
                               capturapokemon(miutu);     
                            }
                            catch(MochilaLlenaException error)   {
                                System.err.println(error.toString());
                            }
                            
                            }
                            else{
                                if(numero==3){
                                    Ekko Ekko = new Ekko((int) (Math.random() * 100) + 1);
                                   try{pokeMochila.capturarPokemon(Ekko);
                                       capturapokemon(Ekko); 
                                   }
                                   catch(MochilaLlenaException error){
                                       System.err.println(error.toString());
                                   }
                                    
                                }
                                else{
                                    if(numero==4){
                                        Charizard Charizard = new Charizard((int) (Math.random() * 100) + 1);
                                       try{ pokeMochila.capturarPokemon(Charizard);
                                          capturapokemon(Charizard);      
                                       }
                                       catch(MochilaLlenaException error){
                                           System.err.println(error.toString());
                                       }
                                       
                                        
                                         
                                        
                                        System.out.println("¿Desea montar en el charizard capturado? (Si/No)");
                                        String montar=teclado.nextLine();
                                        if (montar.equals("Si")){
                                            System.out.println(Charizard.Montar());
                                        }
                                    }
                                }
                            }
                        }
                    
                    
                    
                    
                break;
                
                case("2"):
                    String Opcion;
                    do{
                        System.out.println("==Lista de pokemons==");
                        for (int i = 0; i < pokeMochila.contador; i++){
                            System.out.println( (i+1) + " " + pokeMochila.mostrarPokemon(i).getMote()+ " ("+pokeMochila.mostrarPokemon(i).getEspecie()+ ") -lvl: " + pokeMochila.mostrarPokemon(i).getNivel() );
                        }
                        System.out.println("Opciones (M-Mostrar; L-Liberar; V-Volver):");
                        Opcion =teclado.nextLine();
                        while (!Opcion.equals("M")&&!Opcion.equals("L")&&!Opcion.equals("V")){
                            System.out.println("Opciones (M-Mostrar; L-Liberar; V-Volver):");
                            System.out.println("Introduce un opción correcta");
                            Opcion =teclado.nextLine();
                        }
                        switch(Opcion){

                            case("M"):
                                System.out.println("Introduce el numero del pokemon que desea mostrar");
                                int numeromochila=teclado.nextInt();
                                teclado.nextLine();
                                System.out.println(pokeMochila.mostrarPokemon(numeromochila-1).getMote());
                                System.out.println("==========");
                                System.out.println("Especie: "+pokeMochila.mostrarPokemon(numeromochila-1).getEspecie());
                                System.out.println("Nivel: "+pokeMochila.mostrarPokemon(numeromochila-1).getNivel());
                                System.out.println("Tipo: "+pokeMochila.mostrarPokemon(numeromochila-1).getTipo());
                                System.out.println("PS: "+pokeMochila.mostrarPokemon(numeromochila-1).getPs());
                                System.out.println("Ataque: "+pokeMochila.mostrarPokemon(numeromochila-1).getAtaque());
                                System.out.println("Defensa: "+pokeMochila.mostrarPokemon(numeromochila-1).getDefensa());
                            break;
                            case("L"):
                                System.out.println("Introduce el numero del pokemon que desea liberar");
                                int posicionborrar=teclado.nextInt();
                                teclado.nextLine();
                                pokeMochila.eliminarPokemon((posicionborrar-1));
                                Pokemon ordenaMochila;
                                for (int i=(posicionborrar-1); i<(pokeMochila.contador -1) ;i++){
                                    ordenaMochila=pokeMochila.espacio[(i+1)];
                                    pokeMochila.espacio[i]=ordenaMochila;
                                    pokeMochila.espacio[(i+1)]=null;
                                }
                                System.out.println("Pokemon eliminado");
                                pokeMochila.contador--;
                            break;
                        }
                    }while(!Opcion.equals("V") );
                break;
            }
        }
        while(!opcion1.equals("0") );
    }    
}
        
