package practicapokemon;
public class Ekko extends Pokemon {
        public Ekko (int Nivel, String Mote){
        this.Nivel=Nivel;
        this.Mote=Mote;
        this.Ataque=Nivel*4;
        this.Defensa=Nivel*5;
        this.Ps=Nivel*10;
        this.Especie="Ekko";
        this.Tipo="Psiquico/Sieniestro";
    }
    public Ekko (int Nivel){
        this.Nivel=Nivel;
        this.Mote=Especie;
        this.Ataque=Nivel*4;
        this.Defensa=Nivel*5;
        this.Ps=Nivel*10;
        this.Especie="Ekko";
        this.Tipo="Psiquico/Sieniestro";
    }
}
